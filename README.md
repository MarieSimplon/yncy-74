YNCY Yoga training in Annecy

Mise en ligne du projet / Lien : [https://yncy-74.herokuapp.com/]

Framework : Symfony 4 / Bootstrap theme : Bootswatch Cosmo

<div class="center-align"> 

<img src="https://www.peyclet.com/assets/img/yncy_01.png"/>

<img src="https://www.peyclet.com/assets/img/yncy_02.png"/>

</div>


2019 Projet individuel de fin d'études

Outils choisis : Symfony 4 / API Google / FullCalendar JS

Temps imparti pour le développement : 19 jours répartis sur 5 semaines (fin juillet - mois d'août 2019)

Démarche : 
J'ai imaginé ce projet en pensant à mon propre professeur de yoga, qui aimerait beaucoup ce type d'application (du moins je l'espère). 
J'ai essayé de développer un outil intuitif pour quelqu'un qui n'est pas à l'aise avec les sites Web.

Un professeur de yoga veut un site et souhaite être autonome pour les mises à jour.

*Création d’un site internet pour présenter son parcours et ses sessions de formation.
*Un calendrier en ligne permet à l’élève:
- voir les formations disponibles
- l'ajouter à son propre calendrier Google

*Un accès 'back-office' qui permet au professeur de gérer la réservation de ses sessions de formation et de son profil, qui inclut le téléchargement d’images.

Cette personne habiterait et enseignerait à Annecy, j'ai donc nommé le site web YNCY:
Y signifie Yoga, NCY est le code IATA (Association du transport aérien international) pour Annecy.

Images libres de droits : https://pixabay.com
Artiste : 



