<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Formation;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('summary')
            ->add('start', DateTimeType::class, array(
                'format'=>'d-m-Y H:i:s',
                //'format'=>'yyyy-mm-dd H:i:s',
            ))
            ->add('end', DateTimeType::class, array(
                'format'=>'d-m-Y H:i:s',
            ))
            ->add('location', null, array(
                'help' => 'Veuillez indiquer le lieu, l\'adresse postale et le pays.',
            ))
            ->add('description', null, array(
            ))
            ->add('formations')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
