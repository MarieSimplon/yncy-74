<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Entity\Booking;
use App\Entity\Teacher;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{

    /**
     * @Route(
     * "/{_locale}/admin",
     * name="admin",
     * requirements={"_locale": "en|fr"}
     * )
     */
    
    public function index(Request $request)
    {

        $formations = $this->getDoctrine()
        ->getRepository(Formation::class)
        ->findAll();

        $bookings = $this->getDoctrine()
        ->getRepository(Booking::class)
        ->findAll();

        $teachers = $this->getDoctrine()
        ->getRepository(Teacher::class)
        ->findAll();


// dump($formationsPage);die;
        return $this->render('admin/admin.html.twig', [
            'formations' => $formations,
            'bookings' => $bookings,
            'teachers' => $teachers,
        ]);
    }
}
