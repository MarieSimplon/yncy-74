<?php

namespace App\Controller;

Use App\Entity\Formation;
Use App\Form\FormationType;

//use Doctrine\Common\Persistence\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
//use Doctrine\ORM\EntityManagerInterface;
use App\Repository\FormationRepository;
//use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//use Symfony\Component\Form\FormTypeInterface;

class FormationController extends AbstractController
{
    /**
     * @Route("/admin/formation", name="index_formation")
     */
    public function index(Request $request,PaginatorInterface $paginator) //OK
    {
        $formations = $this->getDoctrine()
        ->getRepository(Formation::class)
        ->findAll();

        // $repoFormation = $this->getDoctrine()->getRepository(Formation::class);
        // $formations = $repoFormation->findAll();
        // dump($formations);die;
        
        $repo2= $this->getDoctrine()
        ->getRepository('App\Entity\Formation')
        ->queryFormation()->getQuery();

        $formationsPage = $paginator
        ->paginate($repo2, $request->query->getInt('page', 1), 3);

        return $this->render('admin/formation/formation.html.twig', [
            'formations' => $formations,
            'formationsPage'=>$formationsPage, 
            // ['pagination' => $pagination]

        ]);
    }

    /**
     * @Route({
     *  "en": "admin/formation/add",
     *  "fr": "admin/formation/ajouter"
     * }, name="add_formation")
     */

    public function add(Request $request, EntityManagerInterface $manager)//OK
    {
        $formation = new Formation();
        $form=$this->createForm(FormationType::class, $formation);   
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $file = $form->get('image')->getData(); //OK
            $image = '/uploads/'.md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $image);
            $formation->setImage($image);

            // $task=$form->getData();
            $manager=$this->getDoctrine()->getManager();
            $manager->persist($formation);
            $manager->flush();

            return $this->redirectToRoute('index_formation');
        }

        return $this->render('admin/formation/add/add.html.twig', [
            'form'=>$form->createView(),
            // 'formations' => $formations
        ]);      
    }

    /**
     * @Route({
     *     "fr": "admin/formation/modifier/{id}",
     *     "en": "admin/formation/edit/{id}"
     * }, name="edit_formation")
     */

    public function edit(Formation $formation, Request $request, $id, EntityManagerinterface $manager){ //OK
        $formation = $this->getDoctrine()
        ->getRepository(Formation::class)
        ->findOneById($id);

        $form=$this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        $repo=$this->getDoctrine()->getRepository(Formation::class)->find($id);
        if($form ->isSubmitted() && $form ->isValid()){

            $file = $form->get('image')->getData(); //OK
            $image = '/uploads/'.md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $image);
            $formation->setImage($image);

            // $task=$form->getData();

            $manager=$this->getDoctrine()->getManager();
            $manager->persist($formation);
            $manager->flush();

            return $this->redirectToRoute('edit_formation', array('id' =>$formation->getId()));
        }

        return $this->render('admin/formation/edit/edit.html.twig', [
            // 'formations' => $formations,
            'formation'=>$formation,
            'form'=>$form->createView(),
            //'id'=>$id

        ]);
     }

     /**
     * @Route({
     *     "fr": "admin/formation/afficher/{id}",
     *     "en": "admin/formation/view/{id}"
     * }, name="view_formation")
     */

     public function view($id) //OK
     {
        $formations=$this->getDoctrine()
        ->getRepository(Formation::class)
        ->findAll();
        $repo=$this->getDoctrine()->getRepository(Formation::class);
        $formation=$repo->find($id);

        return $this->render('admin/formation/view/view.html.twig',[
            'formation'=>$formation,
           // 'formations'=>$formations,
            'id'=>$id
        ]

        );
     }

    /**
     * @Route({
     *      "fr": "admin/formation/{id}/supprimer",
     *      "en": "admin/formation/{id}/delete"
     * }, name="formation_delete")
     */

    public function deleteFormation($id) //OK
     {
        $formationRepo=$this->getDoctrine()->getRepository(Formation::class);
        $formation = $formationRepo->find($id);
        //dump($formation);die;

        $manager=$this->getDoctrine()->getManager();

        $manager->remove($formation); //ATTENTION -> bien penser au cascade remove dans l'entité
        $manager->flush();

        return $this->redirectToRoute('index_formation');
     }


}


