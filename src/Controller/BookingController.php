<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use DateTime;
use Knp\Component\Pager\PaginatorInterface;
use App\ServiceGoogle\ClientFactory2;
use App\Repository\BookingRepository;
use Doctrine\ORM\EntityManagerInterface;
//use Doctrine\Common\Persistence\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class BookingController extends AbstractController
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->bookingRepository = $entityManager->getRepository(Booking::class);
    }
    /**
     * @Route(
     * "/{_locale}/admin/booking/",
     * name="booking_index",
     * methods={"GET"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    public function index(Request $request,BookingRepository $bookingRepository): Response
    {
        $repo= $this->getDoctrine()
        ->getRepository('App\Entity\Booking')
        ->queryBooking()->getQuery();
        // $bookingsPage = $paginator
        // ->paginate($repo, $request->query->getInt('page', 1), 3);

        return $this->render('admin/booking/booking.html.twig', [
            'bookings' => $bookingRepository->findAll(),
            // 'bookingsPage' => $bookingsPage,
        ]);        
    }
    /**
     * @Route(
     * "/{_locale}/admin/booking/calendar",
     * name="booking_calendar",
     * methods={"GET"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    public function calendar(): Response
    {
        return $this->render('admin/booking/calendar.html.twig');
    }
    /**
     * @Route("/{_locale}/admin/booking/redirect", name="booking_redirect")
     */
    public function redirectMe(Request $request): Response
    {
        // dump($request);die;
        $code = $request->query->get('code');
        $this->get('session')->set('googleCode', $code);
        return $this->redirectToRoute('booking_new');//booking_calendar
    }

    /**
     * @Route("/{_locale}/admin/booking/new", name="booking_new", methods={"GET","POST"},requirements={"_locale": "en|fr"})
     * @Route("/{_locale}/admin/booking/{id}/edit", name="booking_edit", methods={"GET","POST"},requirements={"_locale": "en|fr"})
     */

    public function new(Booking $booking = null, Request $request, ClientFactory2 $clientFactory2, EntityManagerInterface $manager): Response //injection de dépendance
    {
        $clientFactory2 = new \App\ServiceGoogle\ClientFactory2();
        
        // Paramètre "code" est envoyé lorsque l'utilisateur donne l'autorisation 
        //d'accès de l'application à l'api. Attention, ne pas oublier le paramétrage de 
        //l'ID client OAuth 2.0 (URI de redirection autorisés) via la console google developers
        $authCode = isset($_GET['code']) ? $_GET['code'] : null;
        $authCode = $this->get('session')->get('googleCode'); //méthode session de Symfony get attribute by name
        
        try {
            // Création du client google pour réaliser des requêtes aux APIs
            $client = $clientFactory2->create([
                'application_name' => 'YNCY-74',
                'scopes'           => [Google_Service_Calendar::CALENDAR_EVENTS, 
                                        // Google_Service_Calendar::CALENDAR
                                    ],
                'auth_code'        => $authCode,
                'auth_config'      => '../credentials.json',
                'token_path'       => '../public/token.json',
            ]);
        }
        catch (\App\ServiceGoogle\AuthRedirectionException $exception) {
            // Redirect chez Google si aucune
            //autorisation d'accès de l'application à l'api
            header('Location:' . $exception->getAuthUrl());
            die();
        }

        if(!$booking){
            $booking = new Booking();
        }
       
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if( !$booking->getId() ){
                $manager = $this->getDoctrine()->getManager();
            }
            $manager->persist($booking);
            $manager->flush();

        // calls the service
        $service = new Google_Service_Calendar($client);

        $calendarId = 'primary';
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();
        //show events (ou pas)
        if (empty($events)) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }
        $dateStart= $booking->getStart();
        $dateStartGF= $dateStart->format('Y-m-d\TH:i:s.u');

        $dateEnd= $booking->getEnd();
        $dateEndGF= $dateEnd->format('Y-m-d\TH:i:s.u');

        $event = new Google_Service_Calendar_Event(array(
            'summary' => $booking->getSummary(),
            'start' => array(
                'format'=>'Y-m-d\TH:i:s.u',
                'label' => 'start',
                'dateTime' => $dateStartGF,
                'timeZone' => 'Europe/Paris',
            ),
            'end' => array( 
                'format'=>'Y-m-d\TH:i:s.u',
                'label' => 'end',
                'dateTime' => $dateEndGF,
                'timeZone' => 'Europe/Paris',
            ),
            'location' => $booking->getLocation(),
            'description' => $booking->getDescription(),
            'formations'=>$booking->getFormations(),
            ));
            dump($event);

        $event = $service->events->insert($calendarId, $event);
        dump($event);
        // printf('Event created: %s\n', $event->htmlLink);

            return $this->redirectToRoute('booking_calendar');
        }
        return $this->render('admin/booking/new.html.twig', [
            //'booking' => $booking,
            'form' => $form->createView(),
            'editMode' => $booking->getId() !== null
        ]);

        
    }

    /**
     * @Route("/{_locale}/admin/booking/{id}", name="booking_show", methods={"GET"},requirements={"_locale": "en|fr"})
     */
    public function show(Booking $booking): Response
    {
        return $this->render('admin/booking/show.html.twig', [
            'booking' => $booking,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/booking/{id}/edit", name="booking_edit", methods={"GET","POST"},requirements={"_locale": "en|fr"})
     */
    /* 
    public function edit(Request $request, $id, EntityManagerInterface $manager): Response
    {
        $booking = $this->getDoctrine()
        ->getRepository(Booking::class)
        ->findOneById($id);

        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('booking_index');
        }

        return $this->render('admin/booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    } */

    /** 
     * @Route("/{_locale}/admin/booking/{id}", name="booking_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Booking $booking): Response
    {
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($booking);
            $manager->flush();
        }

        return $this->redirectToRoute('booking_index');
    }

}
