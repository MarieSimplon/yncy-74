<?php

namespace App\Controller;

use App\Entity\Teacher;
use App\Form\TeacherType;
use App\Repository\TeacherRepository;
use Doctrine\Common\Persistence\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
//use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeacherController extends AbstractController
{
    /**
     * @Route(
     * "/{_locale}/admin/teacher",
     * name="teacher_index",
     * methods={"GET"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    public function index(TeacherRepository $teacherRepository): Response
    {
        return $this->render('admin/teacher/teacher.html.twig', [
            'teachers' => $teacherRepository->findAll(),
        ]);
    }

    /**
     * @Route(
     * "/{_locale}/admin/teacher/new",
     * name="teacher_new",
     * methods={"GET","POST"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    function new (Request $request, ORMEntityManagerInterface $manager): Response {
        $teacher = new Teacher();

        $form = $this->createForm(TeacherType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form->get('image')->getData(); //OK
            $image = '/uploads/'.md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $image);
            $teacher->setImage($image);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($teacher);
            $manager->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/teacher/new.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/{id}", 
     * name="teacher_show", 
     * methods={"GET"},
     * requirements={"_locale": "en|fr"}
     * )
     */

    public function show(Teacher $teacher): Response
    {
        return $this->render('admin/teacher/show.html.twig', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/edit/{id}", 
     * name="teacher_edit", 
     * methods={"GET","POST"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    ///{id}/edit

    public function edit(Request $request, Teacher $teacher, ORMEntityManagerInterface $manager): Response
    {
        $form = $this->createForm(TeacherType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $this->getDoctrine()->getManager()->flush();

            // return $this->redirectToRoute('teacher_index');

            $file = $form->get('image')->getData(); //OK
            $image = '/uploads/'.md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $image);
            $teacher->setImage($image);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($teacher);
            $manager->flush();

            return $this->redirectToRoute('admin');


        }

        return $this->render('admin/teacher/edit.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/{id}", 
     * name="teacher_delete", 
     * methods={"DELETE"},
     * requirements={"_locale": "en|fr"}
     * )
     */
    public function delete(Request $request, Teacher $teacher): Response
    {
        if ($this->isCsrfTokenValid('delete'.$teacher->getId(), $request->request->get('_token'))) {
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($teacher);
            $manager->flush();
        }

        return $this->redirectToRoute('teacher_index');
    }
}
