<?php

namespace App\Controller;

use App\Entity\Teacher;
use App\Entity\Formation;
use App\Entity\Booking;
use App\Repository\TeacherRepository;
use App\Repository\BookingRepository;
use App\Repository\FormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Doctrine\Common\Annotations;
use Doctrine\Common\Persistence\EntityManagerInterface;
//use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class IndexController extends AbstractController
{
    /**
     * @Route(
     * "/{_locale}/",
     * name="index",
     * requirements={
     *         "_locale": "en|fr"}
     * )
     */
    public function index(Request $request): Response
    {
        $repoTeacher= $this->getDoctrine()->getRepository(Teacher::class);
        $teachers = $repoTeacher->findAll();
        return $this->render('index/index.html.twig', [
            'teachers' => $teachers,
            // 'formations' => $formations,
        ]);
        }

    /**
     * @Route(
     * "/{_locale}/formation",
     * name="formation",
     * requirements={
     *         "_locale": "en|fr"}
     * )
     */
    public function formation(Request $request): Response
    {
        $repoFormation= $this->getDoctrine()->getRepository(Formation::class);
        $formations = $repoFormation->findAll();
        return $this->render('index/formation.html.twig', [
            'formations' => $formations,
        ]);
        }

    /**
     * @Route(
     * "/{_locale}/calendar",
     * name="calendar",
     * requirements={
     *         "_locale": "en|fr"}
     * )
     */
    public function calendar(): Response
    {
        return $this->render('index/calendar.html.twig');
    }
    // public function calendar(Request $request): Response
    // {
    //     $repoFormation= $this->getDoctrine()->getRepository(Formation::class);
    //     $formations = $repoFormation->findAll();
    //     return $this->render('index/calendar.html.twig', [
    //         'formations' => $formations,
    //     ]);
    //     }
            
    /**
     * @Route(
     * "/{_locale}/lastEvents",
     * name="lastEvents",
     * requirements={
     *         "_locale": "en|fr"}
     * )
     */
    public function lastEvents(Request $request,BookingRepository $bookingRepository): Response
    {
        $repoBooking= $this->getDoctrine()->getRepository(Booking::class);
        $bookings = $repoBooking->findAll();
        return $this->render('index/lastEvents.html.twig', [
            'bookings' => $bookings,
        ]);
        }

    /**
     * @Route(
     * "/{_locale}/change_locale/{locale}", 
     * name="change_locale",
     * requirements={
     *         "_locale": "en|fr"}
     * )
     */
    public function changeLocale($locale, Request $request)
    {
        // On stocke la langue dans la session
        $request->getSession()->set('_locale', $locale);

        // On revient sur la page précédente
        return $this->redirect($request->headers->get('referer'));
    }

}
