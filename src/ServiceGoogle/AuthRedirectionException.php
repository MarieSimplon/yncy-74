<?php

namespace App\ServiceGoogle;

use Throwable;

/**
 * Class AuthRedirectionException
 *
 * @author Jérôme Fath
 * @package App\ServiceGoogle
 */
class AuthRedirectionException extends \Exception
{
    /**
     * @var string
     */
    private $authUrl;

    /**
     * AuthRedirectionException constructor.
     * @param string $message
     * @param string $authUrl
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message, $authUrl, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->authUrl = $authUrl;
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->authUrl;
    }
}
