<?php

namespace App\ServiceGoogle;

use Google_Client;
use Symfony\Component\OptionsResolver\OptionsResolver;
//ajout
use App\Form\BookingType;
use Google_Service_Calendar;
use Google_Service_Plus;
use Google_Service_Calendar_Event;


/**
 * Class ClientFactory2
 *
 * @author Jérôme Fath
 * @package App\ServiceGoogle
 */
final class ClientFactory2
{
    /**
     * @var array
     */
    private $options;

    /**
     * @param array $options
     * @return Google_Client
     * @throws AuthRedirectionException
     * @throws \Google_Exception
     */
    public function create(array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);

        $client = new Google_Client();
        $client->setApplicationName($this->options['application_name']);
        $client->setScopes($this->options['scopes']);
        $client->setAuthConfig($this->options['auth_config']);
        $client->setClientId('851864769291-e4jtpgfqbfjefekvtn1r0v97ijpe2fde.apps.googleusercontent.com');
        $client->setClientSecret('eUUiUpnaZs9Nrw-rWiwe8Yf-');
        $client->setRedirectUri('http://localhost:8000/admin/booking/redirect');

        $this->decodeAccessToken($client);

        if(!$client->isAccessTokenExpired()) {
            return $client;
        }

        if (!$client->getRefreshToken() && null === $this->options['auth_code']) {
            throw new AuthRedirectionException('Client requires a redirection to the authentication page', $client->createAuthUrl());
        }

        $this->fetchAccessToken($client);

        return $client;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'auth_code'   => null,
            'auth_config' => 'credentials.json',
            'token_path'  => 'token.json',
        ]);

        $resolver->setRequired(['application_name', 'scopes']);

        $resolver
            ->setAllowedTypes('application_name', 'string')
            ->setAllowedTypes('scopes', 'string[]')
        ;
    }

    /**
     * @param Google_Client $client
     */
    private function decodeAccessToken(Google_Client $client)
    {
        if (!file_exists($this->options['token_path'])) {
            return;
        }

        $client->setAccessToken(
            json_decode(file_get_contents($this->options['token_path']), true)
        );
    }

    /**
     * @param Google_Client $client
     * @throws \Exception
     */
    private function fetchAccessToken(Google_Client $client)
    {
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken(
                $client->getRefreshToken()
            );
        }
        else {
            $accessToken = $client->fetchAccessTokenWithAuthCode($this->options['auth_code']);
            $client->setAccessToken($accessToken);

            if (array_key_exists('error', $accessToken)) {
                throw new \Exception(join(', ', $accessToken));
            }
        }

        $this->saveTokenFile($client);
    }

    /**
     * @param Google_Client $client
     */
    private function saveTokenFile(Google_Client $client)
    {
        if (!file_exists(dirname($this->options['token_path']))) {
            mkdir(dirname($this->options['token_path']), 0700, true);
        }

        file_put_contents($this->options['token_path'], json_encode($client->getAccessToken()));
    }
}