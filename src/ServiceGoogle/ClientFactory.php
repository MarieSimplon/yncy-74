<?php

namespace App\ServiceGoogle;

use App\Form\BookingType;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Plus;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_Resource_Events;

class ClientFactory 
{
    public function getClient() 
    {
    //-----QUICKSTART    
    $client = new Google_Client();
    $client->setApplicationName('YNCY Google Calendar API PHP');
    //precise la portée (SET OU ADD? regarder la diff)
    $client->setScopes([
        //Google_Service_Calendar::CALENDAR_READONLY,
        Google_Service_Calendar::CALENDAR_EVENTS
    ]);
    // $client->addScope(Google_Service_Calendar::CALENDAR_EVENTS);

    $client->setAuthConfig('../client_credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    //-----???
    // $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    // $client->setRedirectUri($redirect_uri);
    // $client->setAccessType('offline');
    // $client->setApprovalPrompt('force');

    if (isset($_GET['code'])) {
        $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
    }
 else {
    $authUrl = $client->createAuthUrl();
    print "<a class='login' href='$authUrl'>Connect Me!</a>";
}
    //-----QUICKSTART
    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = '../token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            //$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php');
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';            
            $authCode = trim(fgets(STDIN)); //ATTENTION ATTENTION GERER L'AFFICHAGE ET PAS PERDRE L'APPLI :()

            // $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            // $client->setRedirectUri($redirect_uri);

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }


        return $client;
    }
            
    // // Get the API client and construct the service object.
    // $client = getClient();
    // $service = new Google_Service_Calendar($client);
    
    public function __toString() {
        return $this->title;
    }

    public function testMercredi(){
        $client = new Google_Client();
        $client->setApplicationName('YNCY Google Calendar API PHP');
        $client->setClientId('851864769291-e4jtpgfqbfjefekvtn1r0v97ijpe2fde.apps.googleusercontent.com');
        $client->setClientSecret('eUUiUpnaZs9Nrw-rWiwe8Yf-');
        // $client->setRedirectUri('http://localhost/');
        $client->setScopes([
            //Google_Service_Calendar::CALENDAR_READONLY,
            Google_Service_Calendar::CALENDAR_EVENTS
        ]);
        // $client->setDeveloperKey('xxxxxxxxx');
        $service = new Google_Service_Calendar($client);

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['token'] = $client->getAccessToken();
            header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
        }
          
        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
        }
        // dump($client);die;
        if ($client->getAccessToken()) {
            echo "<hr><font size=+1>I have access to your calendar</font>";

            $event = new Google_Service_Calendar_Event(array(
                // 'summary' => 'Google I/O 2015',
                'title' => 'title',
                // 'start' => array(
                'beginAt' => array( 
                    'format'=>'Y-m-d\TH:i:s.u',
                    'label' => 'beginAt',
                //   'dateTime' => '2019-05-28T09:00:00-07:00',
                //   'timeZone' => 'America/Los_Angeles',
                ),
                'endAt' => array( 
                    'format'=>'Y-m-d\TH:i:s.u',
                    'label' => 'endAt',
                //   'dateTime' => '2019-05-28T09:00:00-07:00',
                //   'timeZone' => 'America/Los_Angeles',
                ),
                'locationName' => 'locationName',
                'address' => 'address',
                'zipCode' => 'zipCode',
                'city' => 'city',
                'country' => 'country',
    
                // 'formations'=>'formations', 
                // 'categories'=>'categories',
                ));
            $calendarId = 'primary';
            $event = $service->events->insert($calendarId, $event);


            // $event = new Google_Service_Calendar_Event();
            // $event->setSummary('Halloween');
            // $event->setLocation('The Neighbourhood');
            // $start = new Google_Service_Calendar_EventDateTime();
            // $start->setDateTime('2018-03-29T10:00:00.000-05:00');
            // $event->setStart($start);
            // $end = new Google_Service_Calendar_EventDateTime();
            // $end->setDateTime('2018-03-29T10:25:00.000-05:00');
            // $event->setEnd($end);
            // $calendarId = 'primary';
            // $events = $cal->events->insert('primary', $event);
            
          $_SESSION['token'] = $client->getAccessToken();
              } else {
                $authUrl = $client->createAuthUrl();
                print "<a class='login' href='$authUrl'>Connect Me!</a>";
              }
                    //   $this->view->render('booking_index');

                    
          
          }
    

    public function entreDoudou(){

    //TRY DEBUT
        // Get the API client and construct the service object.
        $client = new Google_Client();
        $client->setApplicationName('YNCY Google Calendar API PHP');
        $client->setScopes([
            Google_Service_Calendar::CALENDAR_EVENTS
        ]);

        $service = new Google_Service_Calendar($client);

        $event = new Google_Service_Calendar_Event(array(
            // 'summary' => 'Google I/O 2015',
            'title' => 'title',
            // 'start' => array(
            'beginAt' => array( 
                'format'=>'Y-m-d\TH:i:s.u',
                'label' => 'beginAt',
            //   'dateTime' => '2019-05-28T09:00:00-07:00',
            //   'timeZone' => 'America/Los_Angeles',
            ),
            'endAt' => array( 
                'format'=>'Y-m-d\TH:i:s.u',
                'label' => 'endAt',
            //   'dateTime' => '2019-05-28T09:00:00-07:00',
            //   'timeZone' => 'America/Los_Angeles',
            ),
            'locationName' => 'locationName',
            'address' => 'address',
            'zipCode' => 'zipCode',
            'city' => 'city',
            'country' => 'country',

            // 'formations'=>'formations', 
            // 'categories'=>'categories',
            ));
        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);


    return $client;
    //TRY FIN
    }

   

}